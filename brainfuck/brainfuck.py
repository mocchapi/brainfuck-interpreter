#!/usr/bin/python3

'''
Brainfuck interpreter
(Lis)Anne Mocha - 03/02/2021
im gay lol       (DD/MM/YYYY)

https://gitlab.com/mocchapi/Brainfuck-interpreter

This project provides a super inelegant `Brainfuck` class that can interpret Brainfuck (variant) programs

note that the examples given here assume the `Brainfuck` class is imported directly, like this:
`from brainfuck import Brainfuck,Variations,TrivialBrainfuckSubstitution`

The simplest way to use this is to load in a Brainfuck program as a string like this:
 `interpreter = Brainfuck(from_string='+[+[--[+]]]')` # loads the interpreter: program is not executed yet

and then running it automatically by using `Brainfuck.complete()`:
 `interpreter.complete()` # runs the program until it has reached it's end

complete() also accepts an optional `maxloops` argument, which raises `loopsExceededError`
if the program runs more loops than allowed:
 `interpreter.complete(maxloops=10)` # executes at most 10 instructions in the program

alternatively, you can execute the program step-by-step by repeatably calling `Brainfuck.step()`:
 `for i in range(0,10): interpreter.step()` # executes 10 instructions in the program

you can get the value of any cell in the interpreter's tape with `Brainfuck.getval(index=yourposition)`:
 `interpreter.getval(index=10)` # returns value at 10

omitting the index or setting it to `None` will return the value at the *currently selected tape position*
which is arguably more useful:
 `interpreter.getval()` # returns value at current tape position



This interpreter also allows for custom syntax, and is compatible with some BRAINFUCK Variations out of the box
a custom syntax is very simply a dict with the key being the characters your program is using, with the default syntax
it corrosponds with as value, like this:
 `Brainfuck(syntax={'+':'add_one','-':'remove_one', 'key_sep':' '})`

if a key is missing from the custom syntax, like in the example, it will simply use the default syntax for that instruction
this means internally the example up above will look like this:
 `{'+':'add_one', '-':'remove_one', '[':'[', ']':']', '<':'<', '>':'>', '.':'.', ',':',', 'key_sep':'' }`

you might have noticed that the syntax contains a rather odd `'key_sep'` key
this defines how the interpreter will read the program, if loaded from file or string
you do not need to worry about this if all your custom syntax is one character long or if you are loading from a list.
but if you *are* loading from string or file *and* using whole words in your syntax, like in the example, you must supply a seperation character.
using space (`{key_sep:' '}`) is almost always the best choice for single-word syntaxes
a program using the example syntax up above would look like something like this:
 `'add_one > add_one remove_one'`

The function `TrivialBrainfuckSubstitution()` can generate a custom syntax dict for you from the input given:
 `preter = Brainfuck(syntax=TrivialBrainfuckSubstitution('>','<','+','-','.',',','[',']'))`
simply replace the arguments with your own



there are also some buildt in custom syntaxes, under the `Variations` class, which you use as follows:
 `interpreter = Brainfuck(syntax=Variations.OwO)` # uses the OwO syntax
check the class for a full list (or run `print(dict(Variations))`)

if you really like to have headaches, you can generate the syntax of any "Trivial brainfuck Substition" using the `TrivialBrainfuckSubstitution()` function
simply copy any of the code from https://esolangs.org/wiki/Trivial_brainfuck_substitution
the seperator might be slightly different, since seperator the key_sep will be automatically figured out if the `key_sep` argument isnt supplied
a lot of the entries on that page are already in `Variations`


a for-fun function is `string_to_brainfuck` which will just turn the input string (must be ascii) into an executable brainfuck program
you can use the `syntax=` argument to change what syntax it uses
'''


class loopsExceededError(Exception):
	def __init__(self,loopcount):
		super().__init__(f'Program exceeded maximum loop count of {loopcount}')

class Brainfuck():
	def __init__(self,fromstring=None,fromfile=None,fromlist=[],inputtext=':', vocal=True, noinput=False,
					syntax={'key_sep':'','+':'+','-':'-','>':'>','<':'<','.':'.',',':',','[':'[',']':']'}):
		'''initialise with one of the following arguments:
		either fromstring if your brain program is a string,
		fromfile (with filename as a string) if its an external file
		or fromlist where every element in it is a string containing the instruction

		vocal is a bool that enables or disables logging
		note that output (.) instructions will still get printed if this is disabled

		syntax can override the default syntax, in the format of {yourkey:defaultkey}
		for example: {'+':'increment'}
		note that if you want to use custom syntax thats longer than 1 character
		and you're loading from string or file you must also have a `key_sep` key
		in the dictionary that is a seperator character(s), for example:
		{'+':'increment',key_sep='-'}
		where a simple 3 increment program would look like this:
		'increment-increment-increment'
		note that the other custom keys cannot contain the seperator character(s)

		input_text is a simple string that gets put in the input() calls
		'''
		self.funclookup = 	{'+':self.increment,
						'-':self.decrement,
						'>':self.right,
						'<':self.left,
						'.':self.output,
						',':self.input,
						'[':self.openloop,
						']':self.closeloop}
		self.setSyntax(syntax)
		self.logs = ['Legend:','t10 = position 10 on tape','p10 = position 10 in program','v10 = numeric value of 10','-----------------------------']


		self.noinput = noinput

		if fromfile != None:
			self.fromfile(fromfile)
		elif fromstring != None:
			self.fromstring(fromstring)
		elif fromlist != None:
			self.fromlist(fromlist)
		else:
			self.fromstring('')

		self.inputtext = inputtext

		self.program_index = 0
		self.tape_index = 0

		self.tape = {} # dont hate me

		self.vocal = vocal

		self.text = ''

		# self.openloops = []
		self.loops, self.rloops = self.__maploops__()
		if self.vocal: print('\n'.join(self.logs))


	def getlogs(self):
		return self.logs

	def __maploops__(self):
		if self.vocal: print(' Mapping loops...')
		out = {}
		outR = {}
		opens = []
		for index,item in enumerate(self.program):
			thing = self.syntranslate.get(item)

			if thing == '[':
				opens.append(index)
			elif thing == ']':
				if self.vocal: print(f'[p{opens[-1]} <-+-> p{index}]')
				out[opens[-1]] = index
				outR[index] = opens[-1]
				del opens[-1]
		if self.vocal: print(' Loops mapped!')
		return out,outR

	def gettext(self):
		return self.text

	def setSyntax(self,syntax):
		self.syntax = {}
		self.syntranslate = syntax
		for key in self.funclookup:
			newkey = self.syntranslate.get(key,key)
			self.syntax[newkey] = self.funclookup[key]
		self.sep = self.syntranslate.get('key_sep','')


	def fromfile(self,filename):
		with open(filename,'r') as f:
			self.fromstring(f.read())

	def fromstring(self,program):
		tprogram = program
		if self.sep != '':
			tprogram = list(tprogram.split(self.sep))
		else:
			tprogram = list(tprogram)
		self.fromlist(tprogram)

	def fromlist(self,fromlist):
		fromlist.append(' ')
		self.program = fromlist

	def complete(self,maxloops=0):
		loops = 0
		outputs = []
		while self.program_index+1 < len(self.program):
			if maxloops and loops >= maxloops:
				raise loopsExceededError(maxloops)
			self.step()
			loops += 1
		return self.getval()

	def step(self):
		try:
			program_bit = self.program[self.program_index]
			self.syntax.get(program_bit,self.ignore)()
			if self.program_index+1 < len(self.program):
				self.program_index += 1
			return self.getval()
		except Exception as e:
			out = '['+']['.join(self.program[self.program_index-5:self.program_index])+']'
			print(out)
			print(f'{" "*(len(out)-2)}^')
			print(f'@ char {self.program_index}'.center(len(out)))
			raise e




	def increment(self):
		self.__log__('(+) increment', 'v'+str(self.getval()), 'v'+str(self.__wrap__(self.getval() + 1)))
		self.tape[self.tape_index] = self.__wrap__(self.getval() + 1)

	def decrement(self):
		self.__log__('(-) decrement', 'v'+str(self.getval()), 'v'+str(self.__wrap__(self.getval() - 1)))
		self.tape[self.tape_index] = self.__wrap__(self.getval() - 1)

	def right(self):
		self.__log__('(>) right', 't'+str(self.tape_index), 't'+str(self.__wrap__(self.tape_index + 1, minimum=0,maximum=29999)))
		self.tape_index = self.__wrap__(self.tape_index + 1, minimum=0,maximum=29999)

	def left(self):
		self.__log__('(<) left', 't'+str(self.tape_index), 't'+str(self.__wrap__(self.tape_index - 1, minimum=0,maximum=29999)))
		self.tape_index = self.__wrap__(self.tape_index - 1, minimum=0,maximum=29999)

	def output(self):
		character = str(chr(self.getval()))
		self.__log__('(.) output', 'v'+str(self.getval()), '"'+character+'"')
		print(character)
		self.text += character
		return character

	def input(self):
		self.__log__('(,) input', '', '',end='')
		if self.noinput:
			val = 0
		else:
			val = self.__wrap__(ord(input(self.inputtext)[0]))
		self.tape[self.tape_index] = val
		return val

	def openloop(self):
		closer = self.loops[self.program_index]
		if self.getval() != 0:
			pass # >:3
			self.__log__('([) open loop', 'p'+str(self.program_index), f'run till p{closer}')
			# self.openloops.append(self.tape_index)
		else:
			# self.tape_index = self.openloops[-1]-1
			self.__log__('([) open loop', 'p'+str(self.program_index), f'skip to p{closer}')
			self.program_index = closer
			# del self.openloops[-1]

	def closeloop(self):
		if self.getval() != 0:
			opener = self.rloops[self.program_index]
			self.__log__('(]) close loop', 'p'+str(self.program_index), f'run again at p{opener}')
			self.program_index = opener
		else:
			pass # >:3
			self.__log__('(]) close loop', 'p'+str(self.program_index), 'dont run again')

	def ignore(self):
		if self.vocal:
			thing = self.program[self.program_index]
			self.__log__(f'({thing}) ignore', f'"{thing}"', '-')




	def getval(self,index=None):
		# get value at current tape location
		if index == None:
			index = self.tape_index
		getted = self.tape.get(index,0)
		wrapped = self.__wrap__(getted)
		return wrapped

	def __wrap__(self,number,minimum=0,maximum=255):

		if number < minimum:
			return maximum + (number - minimum) + 1
		elif number > maximum:
			return number - maximum-1
		else:
			return number

	def __log__(self,func,old,new,end='\n'):
		text = f'{"P"+str(self.program_index):>4}:{"T"+str(self.tape_index):<4}{func:14} {old:>4} -> {new:<4}'
		self.logs.append(text)
		if self.vocal: print(text,end=end)


def TrivialBrainfuckSubstitution(right='>',left='<',add='+',subtract='-',output='.',inpt=',',openloop='[',closeloop=']',key_sep=None):
	possible_sep = [' ', ', ',',' '-', '--', '_', '\n']
	out = {'<':left,'>':right,'+':add,'-':subtract,'.':output,',':inpt,'[':openloop,']':closeloop}
	if key_sep == None: # this is fucked up i know
		key_sep = ''
		for key in out.values():
			if len(key) > 1: # look at this fucking scope
				for sep in possible_sep:
					is_good = True

					for another_key in out.values():
						if sep in another_key: # oh my g o d
							is_good = False
							continue
					if is_good:
						key_sep = sep
						break
				break
	out['key_sep'] = key_sep
	return out

def reversedict(mydict):
	return {v: k for k, v in mydict.items()}

class Variations():
	Default =		TrivialBrainfuckSubstitution()

	Ternary = 		TrivialBrainfuckSubstitution("01", "00", "11", "10", "21", "20", "02", "12")
	# https://esolangs.org/wiki/Ternary

	ReverseFuck =	TrivialBrainfuckSubstitution("<", ">", "-", "+", ",", ".", "]", "[")
	# https://esolangs.org/wiki/ReverseFuck

	MorseFuck =		TrivialBrainfuckSubstitution(".--", "--.", "..-", "-..", "-.-", ".-.", "---", "...",key_sep=' ')
	# https://esolangs.org/wiki/Morsefuck

	Pikalang =		TrivialBrainfuckSubstitution("pipi", "pichu", "pi", "ka", "pikachu", "pikapi", "pika", "chu")
	# https://esolangs.org/wiki/Pikalang

	Omam = 			TrivialBrainfuckSubstitution("hold your horses now", "sleep until the sun goes down", "through the woods we ran", "deep into the mountain sound", "don't listen to a word i say", "the screams all sound the same", "though the truth may vary", "this ship will carry",key_sep='\n')
	# https://esolangs.org/wiki/Omam

	PenisScript =	TrivialBrainfuckSubstitution("8=D", "8==D", "8===D", "8====D", "8=====D", "8======D", "8=======D", "8========D")
	# https://esolangs.org/wiki/PenisScript

	fuckbeEs = 		TrivialBrainfuckSubstitution("f", "u", "c", "k", "b", "e", "E", "s")
	# https://esolangs.org/wiki/FuckbeEs

	UwU = 			TrivialBrainfuckSubstitution("OwO", "°w°", "UwU", "QwQ", "@w@", ">w<", "~w~", "¯w¯")
	# https://esolangs.org/wiki/UwU

	SCREAMCODE =	TrivialBrainfuckSubstitution("AAAH", "AAAAGH", "FUCK", "SHIT", "!!!!!!", "WHAT?!", "OW", "OWIE")
	# https://esolangs.org/wiki/SCREAMCODE

	GERMAN = 		TrivialBrainfuckSubstitution("RECHTS", "LINKS", "ADDITION", "SUBTRAKTION", "EINGABE", "AUSGABE", "SCHLEIFENANFANG", "SCHLEIFENENDE")
	# https://esolangs.org/wiki/GERMAN

	DetailedFuck =	TrivialBrainfuckSubstitution("MOVE THE MEMORY POINTER ONE CELL TO THE RIGHT", "MOVE THE MEMORY POINTER ONE CELL TO THE LEFT", "INCREMENT THE CELL UNDER THE MEMORY POINTER BY ONE", "DECREMENT THE CELL UNDER THE MEMORY POINTER BY ONE", "REPLACE THE CELL UNDER THE MEMORY POINTER'S VALUE WITH THE ASCII CHARACTER CODE OF USER INPUT", "PRINT THE CELL UNDER THE MEMORY POINTER'S VALUE AS AN ASCII CHARACTER", "IF THE CELL UNDER THE MEMORY POINTER'S VALUE IS ZERO INSTEAD OF READING THE NEXT COMMAND IN THE PROGRAM JUMP TO THE CORRESPONDING COMMAND EQUIVALENT TO THE ] COMMAND IN BRAINFUCK", "IF THE CELL UNDER THE MEMORY POINTER'S VALUE IS NOT ZERO INSTEAD OF READING THE NEXT COMMAND IN THE PROGRAM JUMP TO THE CORRESPONDING COMMAND EQUIVALENT TO THE [ COMMAND IN BRAINFUCK")
	# https://esolangs.org/wiki/VerboseFuck

	Arrows =		TrivialBrainfuckSubstitution('>', '<', '^', 'v', '!', '?', '\\', '/')
	Yell = 			TrivialBrainfuckSubstitution('RIGHT','LEFT','ADD','SUBTRACT','OPEN','CLOSE','PRINT','INPUT')
	Letters =		TrivialBrainfuckSubstitution('R','L','A','S','P','I','O','C')


# '++++++++++' = 10
# '++++++++++'++++++++++'++++++++++'++++++++++'
# '> ++++ [ <++++++++++>- ]<' # 40
# '> +++++++++ [ <+++++>- ]<' # 40

def bfcompress(syn,number,mode='+',recurse=5):
	best = None
	sep = syn.get('key_sep','')
	if recurse <= 0:
		# print('   Compress failed')
		return None
	baseline = (number*(sep+syn[mode]+sep)).strip(sep)
	for i in range(1,number+1):
		div = number / i
		# print(i,div)
		if div == int(div):
			out = [syn['>'],i*syn['+'],syn['['],syn['<'],mode*int(div),syn['>'],syn['-'],syn[']'],syn['<']]
			out = sep.join(out)
			if best:
				if len(out) < len(best):
					best = out
			else:
				best = out

	if best == None or len(best) >= len(baseline):
		# print('   Recurse:',recurse)
		out = bfcompress(syn, number-1,recurse=recurse-1,mode=mode)
		if out:
			out = (out+sep+syn[mode]).strip(sep)
		else:
			out = baseline
		# print(out)
		# print(f'baseline ({len(best)} vs {len(baseline)}) [{baseline}]')
		return out
	else:
		# print(f'   Best found')# ({len(best)} vs {len(baseline)}) [{baseline}]')
		return best


def number_to_brainfuck(number,prevnumber=0,syntax=Variations.Default,compress=True):
	sep = syntax.get('key_sep','')
	outend,outfront = '',''
	# print('target num received:',number)
	if number == prevnumber:
		# print('   Same number')
		# print(prevnumber,'->',number,f'({difference})',out)
		# print()
		return ''
	elif number > prevnumber:
		difference = number-prevnumber
		mode = '+'
	else:
		difference = prevnumber-number
		mode = '-'

	if compress:
		out = bfcompress(syntax,difference,mode=mode)
	else:
		out = difference*(sep+syntax[mode]+sep).strip(sep)

	if sep:
		out = out.strip(sep)

	# print(prevnumber,'->',number,f'({difference})',out)
	# print()
	return out

def string_to_brainfuck(string,syntax=Variations.Default,compress=True):
	out = []
	sep = syntax.get('key_sep','')
	for index,character in enumerate(string):
		if index == 0:
			prev = 0
		else:
			prev = ord(string[index-1])
		charnum = ord(character)
		# print('"'+character+'"',str(charnum)+':')
		if prev == charnum:
			# print('Same character')
			out.append(syntax['.'])
		else:
			out.append(number_to_brainfuck(charnum,syntax=syntax,prevnumber=prev,compress=compress)+sep+syntax['.'])

	return (sep).join(out)

if __name__ == '__main__':
	print('i made the string_to_brainfuck function way more optimised this version')
