import setuptools
setuptools.setup(
    name='mochaBrainFuck',
    version='0.11',
    packages=['brainfuck',],
    long_description=open('readme.txt').read(),
)
