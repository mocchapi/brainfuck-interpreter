Brainfuck interpreter
(Lis)Anne Mocha - 03/02/2021
im gay lol       (DD/MM/YYYY)

https://gitlab.com/mocchapi/Brainfuck-interpreter

This project provides a super inelegant `Brainfuck` class that can interpret Brainfuck (variant) programs 

note that the examples given here assume the `Brainfuck` class is imported directly, like this:
`from brainfuck import Brainfuck,Variations,TrivialBrainfuckSubstitution`

The simplest way to use this is to load in a Brainfuck program as a string like this:
 `interpreter = Brainfuck(from_string='+[+[--[+]]]')` # loads the interpreter: program is not executed yet

and then running it automatically by using `Brainfuck.complete()`:
 `interpreter.complete()` # runs the program until it has reached it's end

complete() also accepts an optional `maxloops` argument, which raises `loopsExceededError`
if the program runs more loops than allowed:
 `interpreter.complete(maxloops=10)` # executes at most 10 instructions in the program

alternatively, you can execute the program step-by-step by repeatably calling `Brainfuck.step()`:
 `for i in range(0,10): interpreter.step()` # executes 10 instructions in the program

you can get the value of any cell in the interpreter's tape with `Brainfuck.getval(index=yourposition)`:
 `interpreter.getval(index=10)` # returns value at 10

omitting the index or setting it to `None` will return the value at the *currently selected tape position*
which is arguably more useful:
 `interpreter.getval()` # returns value at current tape position



This interpreter also allows for custom syntax, and is compatible with some BRAINFUCK Variations out of the box
a custom syntax is very simply a dict with the key being the characters your program is using, with the default syntax
it corrosponds with as value, like this:
 `Brainfuck(syntax={'+':'add_one','-':'remove_one', 'key_sep':' '})`

if a key is missing from the custom syntax, like in the example, it will simply use the default syntax for that instruction
this means internally the example up above will look like this:
 `{'+':'add_one', '-':'remove_one', '[':'[', ']':']', '<':'<', '>':'>', '.':'.', ',':',', 'key_sep':'' }`

you might have noticed that the syntax contains a rather odd `'key_sep'` key
this defines how the interpreter will read the program, if loaded from file or string
you do not need to worry about this if all your custom syntax is one character long or if you are loading from a list.
but if you *are* loading from string or file *and* using whole words in your syntax, like in the example, you must supply a seperation character.
using space (`{key_sep:' '}`) is almost always the best choice for single-word syntaxes
a program using the example syntax up above would look like something like this:
 `'add_one > add_one remove_one'`

The function `TrivialBrainfuckSubstitution()` can generate a custom syntax dict for you from the input given:
 `preter = Brainfuck(syntax=TrivialBrainfuckSubstitution('>','<','+','-','.',',','[',']'))`
simply replace the arguments with your own



there are also some buildt in custom syntaxes, under the `Variations` class, which you use as follows:
 `interpreter = Brainfuck(syntax=Variations.OwO)` # uses the OwO syntax
check the class for a full list (or run `print(dict(Variations))`)

if you really like to have headaches, you can generate the syntax of any "Trivial brainfuck Substition" using the `TrivialBrainfuckSubstitution()` function
simply copy any of the code from https://esolangs.org/wiki/Trivial_brainfuck_substitution
the seperator might be slightly different, since seperator the key_sep will be automatically figured out if the `key_sep` argument isnt supplied
a lot of the entries on that page are already in `Variations`


a for-fun function is `string_to_brainfuck` which will just turn the input string (must be ascii) into an executable brainfuck program
you can use the `syntax=` argument to change what syntax it uses 
